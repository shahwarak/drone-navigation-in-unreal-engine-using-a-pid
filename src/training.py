#!/usr/bin/env python3

import rospy
import json
from geometry_msgs.msg import Quaternion
from data_manipulation import *
from numpy import linalg as LA


def UEmsgs_to_observations(current_state, desired_state, current_vel):
	current_position = [current_state.x, current_state.y, current_state.z, current_state.yaw, current_state.pitch, current_state.roll]
	current_velocity = [current_vel.x, current_vel.y, current_vel.z, current_vel.yaw, current_vel.pitch, current_vel.roll]
	desired_state = [desired_state.x, desired_state.y, desired_state.z, desired_state.yaw]
	diff = [desired_state[i] - current_position[i] for i in range(min(len(desired_state), len(current_position)))]
	observation = np.concatenate((current_position, current_velocity))
	return current_position, desired_state, current_velocity, observation, diff

def check_terminal_state(diff, velocity, crashed, inbound, steps):
	# check if the drone has reached the final state
	if LA.norm([diff[0], diff[1], diff[2]]) < 40 and LA.norm([velocity[0:3]])<5:
		terminal = 1
		reached = True
	# check if the drone is out of bounds - check from Unreal
	# check if the drone has crashed - check from Unreal
	elif crashed == 1 or inbound == 0 or steps == 1:
		terminal = 1
		reached = False
	else:
		terminal = 0
		reached = False
	return terminal, reached

def train(self, current_s, desired_s, current_vel, crashed, inbound, info, traindata, expl_policy):
	#check for terminal state
	prvs_terminal = info.prev_terminal
	action = Quaternion()

	current_state, desired_state, current_velocity, observation, diff = UEmsgs_to_observations(current_s, desired_s, current_vel)
	terminal, reached = check_terminal_state(diff, current_velocity, crashed, inbound, info.steps)
	global wp

	if terminal == 1:
		if prvs_terminal == False:
			print('Steps:' + str(info.steps))
			print('InBound:' + str(inbound))
			print('Crashed:' + str(crashed))
			print('Reached: ' + str(reached))
			if info.episode_counter <= info.episode_number:
				info.episode_counter = info.episode_counter + 1
				print("Episode " + str(info.episode_counter))
			else:
				info.episode_val_counter = info.episode_val_counter + 1
				print("Episode " + str(info.episode_val_counter))
			print("DONE TAKING ", info.steps_rollout_counter, " STEPS.")
			traindata.waypoint.append(np.array(wp))
			traindata.steps.append(info.steps_rollout_counter - 41)
			traindata.epi_observations.append(np.array(traindata.observations))
			traindata.epi_actions.append(np.array(traindata.actions))
			traindata.observations = []
			traindata.actions = []

		else:
			pass
		info.steps_rollout_counter = 0
	else:
		if info.episode_counter <= info.episode_number:
			inputs, action = expl_policy.exploration_policy()
			traindata.observations.append(observation)
			traindata.actions.append(np.array(inputs))
			wp = desired_state
			pass

		else:
			if (info.episode_val_counter == 0) and (info.steps_rollout_counter == 0):
				action.x = 0
				action.y = 0
				action.z = 0
				action.w = 0

				dataX, dataY = generate_training_data_inputs(traindata.epi_observations, traindata.epi_actions)
				dataZ = generate_training_data_outputs(traindata.epi_observations)
				np.save(traindata.save_dir + '/training_data/dataX.npy', dataX)
				np.save(traindata.save_dir + '/training_data/dataY.npy', dataY)
				np.save(traindata.save_dir + '/training_data/dataZ.npy', dataZ)
				np.save(traindata.save_dir + '/training_data/training_waypoints.npy', traindata.waypoint)
				np.save(traindata.save_dir + '/training_data/episodes_steps.npy', traindata.steps)
				traindata.observations = []
				traindata.actions = []
				traindata.output = []
				traindata.epi_observations = []
				traindata.epi_actions = []
			if info.episode_val_counter <= info.episode_val:
				inputs, action = expl_policy.exploration_policy()
				traindata.observations.append(observation.tolist())
				traindata.actions.append(inputs)

			else:
				action.x = 0
				action.y = 0
				action.z = 0
				action.w = 0

				with open(traindata.save_dir + '/training_data/states_val.json', 'w') as file:
					serialized_data = [arr.tolist() for arr in traindata.epi_observations]
					json.dump(serialized_data, file)
				with open(traindata.save_dir + '/training_data/controls_val.json', 'w') as file:
					serialized_data = [arr.tolist() for arr in traindata.epi_actions]
					json.dump(serialized_data, file)
				rospy.signal_shutdown("Done")

		info.steps_rollout_counter = info.steps_rollout_counter + 1
	if info.steps_rollout_counter < info.steps_rollout and info.steps_rollout_counter > 0:
		info.steps = bool(0)
	elif prvs_terminal == False:
		info.steps = bool(1)
		self.steps.publish(info.steps)
	elif info.simu_status == 1:
		info.steps = bool(0)
	else:
		info.steps = bool(1)
	info.prev_terminal = bool(terminal)

	return action