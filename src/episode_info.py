#!/usr/bin/env python3

import yaml
import os
import numpy as np
class episodeInfo():

    def __init__(self):
        pass

    prev_terminal = bool(0)
    episode_counter = 0
    episode_number = 2
    episode_val = 0
    episode_val_counter = 0
    steps_rollout = 3000
    steps_rollout_counter = 0
    steps = bool(0)
    simu_status = bool(1)
