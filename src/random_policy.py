import rospy
import math
from geometry_msgs.msg import Quaternion
import numpy as np
from pubsub_synced import desired_xyz
from pubsub_synced import current_xyz
from PID_controller import PIDController
inputs = Quaternion()
import  random
class random_policy(object):
	
	def __init__(self):
		# self.epi_num = episode_num
		self.p_freq = 100
		self.current = np.array(current_xyz)
		self.desired = np.array(desired_xyz)
		self.inputs = Quaternion()
		self.low_val= [-1,-1,-1,-1]
		self.low_val = [num * 0.05 for num in self.low_val]
		self.high_val= [1,1,1,1]
		self.high_val = [num * 0.05 for num in self.high_val]
		self.shape = [4]
		self.rp_count = 0
		self.PID_z = PIDController(1, 0, 180)
		self.PID_pitch = PIDController(1.1, 0, 600)
		self.PID_roll = PIDController(1.2, 0, 400)
		self.PID_yaw = PIDController(3,0,1000)
		self.epsilon = 0.1

	def PIDcontrol(self):
		current = current_xyz
		desired = desired_xyz

		z_rotation_angle = -current.yaw

		x_rotated_curr = current.x*np.cos(np.radians(z_rotation_angle)) - current.y*np.sin(np.radians(z_rotation_angle))
		y_rotated_curr = current.x * np.sin(np.radians(z_rotation_angle)) + current.y * np.cos(np.radians(z_rotation_angle))

		x_rotated_des = desired.x * np.cos(np.radians(z_rotation_angle)) - desired.y * np.sin(
			np.radians(z_rotation_angle))
		y_rotated_des = desired.x * np.sin(np.radians(z_rotation_angle)) + desired.y * np.cos(
			np.radians(z_rotation_angle))
		# print("Desired X: " + str(y_rotated_des))
		# print('Current X:' + str(y_rotated_curr))

		desired_roll = math.degrees(math.atan2((y_rotated_des - y_rotated_curr), (desired.z - current.z)))
		desired_pitch = math.degrees(math.atan2((x_rotated_des - x_rotated_curr), (desired.z - current.z)))

		inputs.w = self.PID_z.compute(desired.z, current.z, 2866) #norm is the heght of the bounds
		inputs.y = self.PID_pitch.compute(x_rotated_des, x_rotated_curr,10000)
		#choice to actuate yaw or not
		inputs.z = 0
		# inputs.z = self.PID_yaw.compute(0, current.yaw, 180)
		inputs.x = self.PID_roll.compute(y_rotated_des,y_rotated_curr, 10000)
		action = [inputs.x, inputs.y, inputs.z, inputs.w]
		return action, inputs

	def exploration_policy(self):

		self.rp_count +=1

		min_action = -1
		max_action = 1
		inputs, action = self.PIDcontrol()
		inputs = np.clip(inputs, min_action, max_action)
		action.x = max(min(action.x, max_action), min_action)
		action.y = max(min(action.y, max_action), min_action)
		action.z = max(min(action.z, max_action), min_action)
		action.w = max(min(action.w, max_action), min_action)
		return inputs, action

