#!/usr/bin/env python3

import time
from pubsub_synced import *
from training import train
import argparse
from random_policy import random_policy
from data_save import data_save


expl_policy = random_policy()


def sub6_callback(self, msg):

	if self.flagdy == True:
		value = msg.data
		y_f = float(value)
		desired_xyz.z = y_f
		begin = time.time()
		# print("callbackFINALSTART")
		self.flagdz = True
		self.flagdy = False

		inputs = train(self, current_xyz, desired_xyz, current_vel, drone_status.crashed, drone_status.inbound, epi_info,
				   traindata, expl_policy)
		end = time.time()
		self.pause.publish(False)
		self.chatter_pub.publish(inputs)
		time_action = end - begin
		# # sleep_time = (1/60) - time_action
		sleep_time = 1/60 #Unreal's framerate
		rospy.sleep(sleep_time)
		self.pause.publish(True)
		self.flags_reset()



def main():

	parser = argparse.ArgumentParser()
	parser.add_argument('--run_num', type=int, default=100)
	args = parser.parse_args()
	global RUN_NUMBER
	RUN_NUMBER = args.run_num
	global traindata
	traindata = data_save(RUN_NUMBER)


	rospy.init_node("training_data")
	PubSub.sub6callback = sub6_callback

	controller = PubSub("chatter", "steps", "current_x", "current_y", "current_z", "desired_location",
						"desired_y", "desired_z", "current_yaw", "crashed", "inbound", 'roll', 'pitch',
						'sim_satus', 'pause','node_status', "velocity_x", "velocity_y", "velocity_z",
						"angvelocity_x", "angvelocity_y", "angvelocity_z",1)
	print("Got here")

	while not rospy.is_shutdown():
		pass

	rospy.signal_shutdown("Done getting data")


if __name__ == "__main__":
	try:
		main()
	except rospy.ROSInterruptException:
		pass

