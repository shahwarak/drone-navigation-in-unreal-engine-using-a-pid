#!/usr/bin/env python3
import os

class data_save():
    def __init__(self, run_num):
        self.epi_observations = []
        self.epi_actions = []
        # self.epi_observations = []
        self.observations = []
        self.actions = []
        self.output= []
        self.rewards = []
        self.waypoint = []
        self.steps = []
        self.save_dir = 'run_' + str(run_num)
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
            os.makedirs(self.save_dir + '/losses')
            os.makedirs(self.save_dir + '/models')
            os.makedirs(self.save_dir + '/saved_forwardsim')
            os.makedirs(self.save_dir + '/saved_trajfollow')
            os.makedirs(self.save_dir + '/training_data')

