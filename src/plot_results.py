#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import json
from numpy import linalg as LA
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--run_num', type=int, default=100)
    parser.add_argument('--epi_num', type=int, default=2)


    args = parser.parse_args()
    save_dir = 'run_' + str(args.run_num)
    epi = args.epi_num

    dataX= np.load(save_dir + '/training_data/dataX.npy')
    dataY= np.load(save_dir + '/training_data/dataY.npy')
    dataZ= np.load(save_dir + '/training_data/dataZ.npy')
    waypoints = np.load(save_dir + '/training_data/training_waypoints.npy')
    steps_per_epi = np.load(save_dir + '/training_data/episodes_steps.npy')


    l = steps_per_epi[epi-1]
    s = np.zeros(l)
    x = np.zeros(l)
    y = np.zeros(l)
    z = np.zeros(l)
    dist = np.zeros(l)
    vel = np.zeros(l)
    data_index = sum(steps_per_epi[:epi-1])
    current = np.array(dataX[data_index:data_index+l])

    xc = current[:,0]
    yc = current[:,1]
    zc = current[:,2]
    xv = current[:,6]
    yv = current[:, 7]
    zv = current[:, 8]
    waypoint = waypoints[epi-1]

    for i in range(l):
        s[i] = i
        dist[i] = LA.norm([(waypoint[0]- xc[i]), (waypoint[1] - yc[i]), (waypoint[2]- zc[i])])
        vel[i] = LA.norm([xv[i], yv[i], zv[i]])


    plt.figure()
    plt.axhline(y=waypoint[0], color='b', linestyle=':')
    plt.plot(s, xc, '.')

    plt.xlabel("Steps")
    plt.ylabel("Distance")
    plt.title("X-axis")

    plt.figure()
    plt.axhline(y=waypoint[1], color='b', linestyle=':')
    plt.plot(s, yc, '.')
    plt.xlabel("Steps")
    plt.ylabel("Distance")
    plt.title("Y-axis")

    plt.figure()
    plt.axhline(y=waypoint[2], color='b', linestyle=':')
    plt.plot(s, zc, '.')
    plt.xlabel("Steps")
    plt.ylabel("Distance")
    plt.title("Z-axis")

    plt.figure()
    plt.plot(s, dist, '.')
    plt.axhline(y=0, color='b', linestyle=':')
    plt.axhline(y=100, color='b', linestyle=':')


    plt.xlabel("Steps")
    plt.ylabel("Distance")
    plt.title("Total distance to the waypoint")

    plt.figure()
    plt.plot(vel)
    plt.xlabel("Steps")
    plt.ylabel("Speed")
    plt.title("Total Speed")

    plt.show()

main()
