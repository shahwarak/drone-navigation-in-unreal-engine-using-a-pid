# Drone Navigation in Unreal Engine using a PID Controller

This ROS package controls a physically simulated drone in Unreal Engine using a PID controller.

  

## Description

The package uses a class of publishers and subscribers to communicate with Unreal Engine where the drone is simulated. The same number of publishers and subscribers are defined in Unreal Engine's blueprint.

  

## System Requirements

The application was developed using the following:

- Ubuntu 20.04

- ROS Noetic

- Unreal Engine 4

  

## Unreal Engine Setup

A drone is physically simualted in Unreal Engine. Users can use existing packages or build one on their own. This package has a thrust-attitude controller for the simulated drone.

  

Control from the player controller is replaced by the messages recieved on control topics being subscribed to in Unreal Engine and published to from the offboard controller in ROS.

  

## Unreal-ROS Connection

This [plugin](https://github.com/code-iai/ROSIntegration) was used to setup the Unreal-ROS conenction.

  

## Controller Setup

The offboard controller will run the roscore and rosbridge_server. The PubSub class defines all the required topics, publishers and subscribers. Data synchronization is added using flags and simulation pause/unpause fucntionality, without the need for timestamp matching.
  

The PID controller is tuned experimentally.
 


## Usage

Once the Unreal-ROS connection is successfully established, simply run the follwing commands:

	$ cd ~/path/to/the/package/src

	$ ./basic_pubsub_v2

The run number can be specified by adding `--run_number NUMBER` when running the `basic_pubsub_v2 ` executable, where NUMBER identifies the run.

Number of episodes to be run can be specified in the `episode_info.py` script. 

The data from each run in saved in the training_data folder of that run, along with some validation data which can be used to evaluate a machine learning model. `run_100` is a sample run provided.

Results from a run can be plotted by specifying the run number and episode number in the following command:

	$ ./plot_results.py --run_num=100 --epi_num=2

A sped up video and plots of a sample run are provided.
 
